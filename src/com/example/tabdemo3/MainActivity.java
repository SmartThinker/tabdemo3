package com.example.tabdemo3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;
import android.widget.TextView;

public class MainActivity extends Activity {
	private TabHost tabHost;
	private Button tab3_button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		tab3_button = (Button) findViewById(R.id.button);
		tab3_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent();
				intent.setClass(MainActivity.this, Other.class);
				startActivity(intent);
			}
		});
		
		tabHost = (TabHost) findViewById(R.id.tabHost);
		tabHost.setup();
		tabHost.addTab(tabHost
				.newTabSpec("tabFirst")
				.setContent(R.id.tabFirst)
				.setIndicator(
						"换乘方案",
						getResources()
								.getDrawable(android.R.drawable.ic_delete)));
		tabHost.addTab(tabHost
				.newTabSpec("tabSecond")
				.setContent(R.id.tabSecond)
				.setIndicator(
						"站点查询",
						getResources().getDrawable(
								android.R.drawable.ic_dialog_alert)));
		tabHost.addTab(tabHost
				.newTabSpec("tabThird")
				.setContent(R.id.tabThird)
				.setIndicator(
						"车辆查询",
						getResources().getDrawable(
								android.R.drawable.ic_input_delete)));
		tabHost.setCurrentTab(0);
		tabHost.setOnTabChangedListener(new OnTabChangeListener()
     {
		@Override
		public void onTabChanged(String tabId) {
		//	<!--点击触发改变Tab标签字体 -->
	//		Integer taInteger=Integer.parseInt(tabId);
	//	tabHost.setCurrentTab(taInteger);
			   /**
		     * 更新Tab标签的颜色，和字体的颜色
		     * @param tabHost
		     */ 
		    for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) { 
	            View view = tabHost.getTabWidget().getChildAt(i); 
	            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); 
	            tv.setTextSize(16); 
	            tv.setTypeface(Typeface.SERIF, 2); // 设置字体和风格  
	            if (tabHost.getCurrentTab() == i) {//选中  
	             //   view.setBackgroundDrawable(getResources().getDrawable(R.drawable.category_current));//选中后的背景  
	                tv.setTextColor(getResources().getColorStateList( 
	                        android.R.color.holo_blue_light)); 
	            } else {//不选中  
	          //      view.setBackgroundDrawable(getResources().getDrawable(R.drawable.category_bg));//非选择的背景  
	                tv.setTextColor(getResources().getColorStateList( 
	                        android.R.color.black)); 
	            } 
	        } 
	    } 
			
     });
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
